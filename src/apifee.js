'use strict';

const Web3    = require('web3');
const Account = require('eth-lib/lib/account')

// ---

module.exports = function apifee(
  abi,
  ethereumProvider,
  privateKey,
  subscriptionContractAddress,
  _logger,
) {
  const logger = _logger || console;

  let web3;
  let credits = 0;

  return {
    initializeClient,
    getCreditsCount,
    getNextRequestHeaders,
    rollbackCreditCount
  };

  function initializeClient() {
    logger.info('Initializing APIFee ...');
    web3      = new Web3(new Web3.providers.HttpProvider(ethereumProvider));
  }

  function getNextRequestHeaders() {
    credits++;

    const messageHash    = web3.utils.soliditySha3(subscriptionContractAddress, credits)
    const ethMessage     = web3.utils.toHex('\x19Ethereum Signed Message:\n32') + messageHash.substring(2)
    const hash           = web3.utils.sha3(ethMessage)
    const localSignature = Account.sign(hash, privateKey)

    const v = web3.utils.hexToNumber('0x' + localSignature.slice(130, 132))

    const signatureParts = {
      r: localSignature.slice(0, 66),
      s: '0x' + localSignature.slice(66, 130),
      v:  v >= 27 ? v : v + 27
    }

    return {
      'X-APIFee-Contract-Address': subscriptionContractAddress,
      'X-APIFee-Credits':          credits,
      'X-APIFee-Signature':        localSignature
    }
  }

  function getCreditsCount(){
    return credits;
  }

  function rollbackCreditCount() {
    --credits;
  }

  function wrapRequest(req) {
    return req;
  }
};
