# SDK Node

const apifeeFactory = require('@apifee/sdk-node').factory;
const apifeeFactory = require('../sdk-node').factory;

const apifee        = apifeeFactory(
  console,
  'abi',
  'ethereumProvider',
  'privateKey',
  'subscriptionContractAddress'
);

apifee.initializeClient();

apifee.wrap(agent('GET', apiUrl)).then()